import { Link } from 'react-router-dom';  


const DetailsHeader = ({ artistId, artistData, songData }) => {
const artist =  artistData?.artists[artistId]?.attributes.name

return (
  <div className='relative w-full flex flex-col mb-10'>
    <div className='w-full bg-gradient-to-l from-transparent to-black sm:h-48 h-28'></div>
    <div className='absolute inset-0 flex items-center'>
      <img src={artistId ? artist.artwork?.url
            .replace('{w}', '500')
            .replace('{h}', '500')
            : songData?.images?.coverart}

            className="sm:w-48 w-28 sm:h-48 h-28 rounded-full object-cover 
            border-slate-900 shadow-2xl shadow-black" alt="art"/> 

    <div className='ml-5'>
        <p className='font-bold text-2xl sm:text-3xl mt-2 text-white'>
        {artistId ? artist?.name : songData?.title}  
        </p>
        {!artistId && (
          <Link to={`/artists/${songData?.artists[0].adamid}`}>
          <p className='text-base text-gray-400 mt-2'>{songData?.subtitle}</p>
          </Link>
        )}
        <p className='text-base font-semibold text-gray-200 mt-2'>
          {
            artistId ? artist?.genreNames[0]: songData?.genres?.primary}
        </p>
    </div>
    </div>
  </div>
)};

export default DetailsHeader;
